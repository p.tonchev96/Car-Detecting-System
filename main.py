import cv2
import datetime
# Capture frames from a video
cap = cv2.VideoCapture('video.mp4')

# Trained XML classifiers for car features
car_cascade = cv2.CascadeClassifier('cars.xml')

try:
    while True:
        # Capture frame-by-frame
        ret, frames = cap.read()

        # If the frame has not been retrieved it's the end of the video
        if not ret:
            break

        # Convert to gray scale
        gray = cv2.cvtColor(frames, cv2.COLOR_BGR2GRAY)

        # Smoothen the img to test if it classifies better
        # gray = cv2.GaussianBlur(gray, (21, 21), 0)

        # Detect cars of diffrent sizes in the input image
        cars = car_cascade.detectMultiScale(gray, 1.3, 1)

        # Draw a bounding rectangle of each car
        for (x,y,w,h) in cars:
            cv2.rectangle(frames, (x,y), (x+w, y+h), (0,0,255), 2)

        if cars.size > 0:
            time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            cv2.imwrite("{}.jpg".format(time),frames)

        # Display the frame
        cv2.imshow('video', frames)

        # Wait for Esc key to stop
        if cv2.waitKey(33) == 27:
            break

finally:
    # When done, release the Capture
    cap.release()
    cv2.destroyAllWindows()
